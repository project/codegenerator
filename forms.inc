<?php
/**
 * Generates form elements
 */
function codegenerator_forms_form($form, &$form_state) {
  $form['include_edit'] = array('#type' => 'checkbox', '#title' => t('Include edit'));
  $form['form_name'] = array('#type' => 'textfield', '#title' => t('Form name'));
  $form['form'] = array('#theme' => 'cr_states_table');
  $cnt = 0;
  for ($cnt = 0; $cnt < 20; $cnt++) {
    $form['form'][$cnt]["name-$cnt"] = array('#type' => 'textfield', '#title' => t('name'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["form_type-$cnt"] = array('#type' => 'textfield', '#title' => t('#type'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["title-$cnt"] = array('#type' => 'textfield', '#title' => t('#title'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["required-$cnt"] = array('#type' => 'select', '#title' => t('#required'),
      '#options' => array('FALSE', 'TRUE',),
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["default_value-$cnt"] = array('#type' => 'textfield', '#title' => t('#default_value'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["value-$cnt"] = array('#type' => 'textfield', '#title' => t('#value'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["size-$cnt"] = array('#type' => 'textfield', '#title' => t('#size'),
      '#size' => 3,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["maxlength-$cnt"] = array('#type' => 'textfield', '#title' => t('#maxlength'),
      '#size' => 6,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["prefix-$cnt"] = array('#type' => 'textfield', '#title' => t('#prefix'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["suffix-$cnt"] = array('#type' => 'textfield', '#title' => t('#suffix'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['form'][$cnt]["submit-$cnt"] = array('#type' => 'textfield', '#title' => t('#submit'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $prev_cnt = $cnt;
  }
  $form['btn_submit'] = array('#type' => 'submit', '#value' => t('Generate form'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_form',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  );  
  
  return $form;
}

function codegenerator_get_form($form, &$form_state) {
  $include_edit = $form_state['values']['include_edit'];
  $menus = array();
  $values = $form_state['values'];
  $elements = array();
  $spec_fields = array('form_type', 'title', 'default_value', 'value', 'size', 'maxlength', 'prefix', 'suffix', 'submit', 'required');
  $spec = array();
  $edit_fetch = '';
  $cnt = 0;
  if ($include_edit) {
    $edit_fetch = "$" . "edit = db_select('" . $form_state['values']['table'] . 
      "', 'tbl')" . "\n    " . "->fields('tbl', array('";
    $keyfield = '';
    while(isset($values["name-$cnt"])) {
      if (!empty($values["name-$cnt"])) {
        $efld[] = $values["name-$cnt"];
        if ($form['form'][$cnt]["name-$cnt"]['#primary_key']) {
          $keyfield = $values["name-$cnt"];
        }
      }
      $cnt++;
    }
    $edit_fetch .= implode("', '", $efld) .
      "'))" . "\n    " . "->condition('$keyfield', $" . $keyfield . ")" .
       "\n    " ."->execute()->fetchObject();";
  }
  $cnt = 0;
  while(isset($values["name-$cnt"])) {
    $name = $values["name-$cnt"];
    if (!empty($name)) {
      foreach ($spec_fields as $fld) {
        switch ($fld) {
          case 'form_type':
            switch ($values["form_type-$cnt"]) {
              case 'markup':
                $spec[] = "'#markup' => '" . $values["value-$cnt"] . "'";
                break;
              case 'date_popup':
                $spec[] = "'#date_format' => variable_get('date_format_short', 'm/d/Y H:i')";
                //deliberate fall through
              default:
                $spec[] = "'#type' => '" . $values["form_type-$cnt"] . "'";
                break;
            }
            break;
            
          case 'title':
            if (!empty($values["$fld-$cnt"])) {
              $spec[] = "'#title' => t('" . $values["$fld-$cnt"] . "')";
            }
            break;
            
          case 'value':
            switch ($values["form_type-$cnt"]) {
              case 'markup':
                $spec[] = "'#markup' => t('" . $values["$fld-$cnt"] . "')";
                break;

              case 'hidden':
                break;

              default:
                if (!empty($values["$fld-$cnt"])) {
                  $spec[] = "'#$fld' => t('" . $values["$fld-$cnt"] . "')";
                }
                break;

            }
            break;

          case 'default_value':
            if ($include_edit) {
              switch ($values["form_type-$cnt"]) {
                  case 'markup':
                  case 'submit':
                    break;
                    
                  case 'date_popup':
                    $edit_default = "date('Y-m-d', $" . "edit->$name)";
                    $other_default = empty ($values["$fld-$cnt"])? "''" : $values["$fld-$cnt"];
                    $tfld = $fld;
                    break;
                    
                  case 'hidden':
                    $tfld = 'value';
                    $edit_default = "$" . "edit->$name";
                    $other_default = "'" . $values["$fld-$cnt"] . "'";
                    break;

                  default:
                    $tfld = $fld;
                    $edit_default = "$" . "edit->$name";
                    $other_default = "'" . $values["$fld-$cnt"] . "'";
                    break;
                }
                $spec[] = "'#$tfld'" . " => isset($" . "edit)? " . $edit_default . ' : ' . $other_default;
              break;
            }
          case 'size':
          case 'maxlength' :
          case 'prefix':
          case 'suffix':
            if (!empty($values["$fld-$cnt"])) {
              $spec[] = "'#$fld' => '" . $values["$fld-$cnt"] . "'";
            }
            break;
            
          case 'submit':
            if (!empty($values["$fld-$cnt"])) {
              $spec[] = "'#$fld' => array('" . $values["$fld-$cnt"] . "')";
            }
            break;
            
          case 'required':
            if ($values["$fld-$cnt"]) {
              $spec[] = "'#$fld' => TRUE";
            }
            break;
        }
      }
      $elements[] = "$" . "form['$name'] = array(\n    " . implode(",\n    ", $spec) . ",\n  );";
      $spec = array();
    }
    $cnt++;
  }
  $btns = array('btn_submit' => 'Save', 'btn_cancel' => 'Cancel', 'btn_delete' => 'Delete');
    
  $btns = "\n" . "\n  " . '$' . "form['btn_submit'] = array(" .
    "\n    " . "'#type' => 'submit'," .
    "\n    " . "'#value' => ". "t('Save')," .
    "\n  " . ');';
  $btns .= "\n  " . '$' . "form['btn_Cancel'] = array(" .
    "\n    " . "'#type' => 'submit'," .
    "\n    " . "'#value' => ". "t('Cancel')," .
    "\n  " . ');';
  if ($include_edit) {
    $btns .= "\n  " . "if (!empty($" . $keyfield . ")) {";
    $btns .= "\n    " . '$' . "form['btn_delete'] = array(" .
      "\n      " . "'#type' => 'submit'," .
      "\n      " . "'#value' => ". "t('Delete')," .
      "\n    " . ');';
    $btns .= "\n  " . "}";
  }
    
  if (!empty($elements)) {
    $form['ajx']['#value'] = 'function ' . $form_state['values']['form_name'] .
      '($form, &$form_state' . ($include_edit? ', $' . $keyfield : '') . ') {' . 
      "\n  " . $edit_fetch .
      "\n  " . implode("\n  ", $elements) .
      $btns . "\n" .
      "\n  " . "return $" . 'form;'  .
      "\n" . '}' .
      "\n" . "\n" .
      "/**" . "\n" . ' * Validate function' . "\n" . ' */' . "\n" .
      'function ' . $form_state['values']['form_name'] . '_validate($form, &$form_state) {' .
      "\n" .
      "\n}" . "\n" . "\n" .
      "/**" . "\n" . ' * Submit function' . "\n" . ' */' . "\n" .
      'function ' . $form_state['values']['form_name'] . '_submit($form, &$form_state) {' .
      "\n" .
      "\n}".
      "\n";
      
  }
  return $form['ajx'];
}

/**
 * generate form by table name
 */
function codegenerator_forms_bytable_form($form, &$form_state) {
  $form['form_name'] = array('#type' => 'textfield', '#title' => t('Form name'));
  $form['cont'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['cont']['table'] = array(
    '#type' => 'textfield',
    '#title' => t('Tablename'),
    '#required' => TRUE,
  );
  $form['cont']['btn_get_schema'] = array('#type' => 'button', '#value' => t('Get schema'),
    '#ajax' => array(
      'callback' => 'codegenerator_forms_get_table_schema',
      'wrapper' => 'ajax_schema_div',
    ),
  );
  $form['dmy1'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['form'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="ajax_schema_div">',
    '#suffix' => '</div>',
    '#theme' => 'cr_form_table'
  );
  if (isset($form_state['values']) && !empty($form_state['values']['table'])) {
    $schema = drupal_get_schema($form_state['values']['table']);    
    module_load_include('install', $schema['module']);
    $result = call_user_func($schema['module'] . '_schema');
    $schema = $result[$form_state['values']['table']];
    $cnt = 0;
    $form['form']['include_edit'] = array('#type' => 'checkbox', '#title' => t('Include edit'));
    $fld_types = array(
      'textfield' => t('Textfield'), 'select' => t('Select'), 'checkbox' => t('Checkbox'),
      'checkboxes' => t('Checkboxes'), 'radios' => t('Radios'), 'date_popup' => t('Date Popup'),
      'textarea' => t('Textarea'), 'hidden' => t('Hidden'), 'markup' => t('Markup'),
      'others' => t('Others'),
    );
    foreach ($schema['fields'] as $field => $fld_spec) {
      $form['form'][$cnt]["name-$cnt"] = array(
        '#type' => 'checkbox',
        '#field_suffix' => $field,
        '#description' => t('Type: %type', array('%type' => $fld_spec['type'])),
        '#title' => t('Select field'),
        '#return_value' => $field,
        '#primary_key' => ($fld_spec['type'] == 'serial'),
      );
      $form['form'][$cnt]["form_type-$cnt"] = array('#type' => 'select', '#title' => t('#type'),
         '#options' => $fld_types, '#default_value' => ($fld_spec['type'] === 'serial'? 'hidden' : 'textfield')
      );
      $form['form'][$cnt]["title-$cnt"] = array('#type' => 'textfield', '#title' => t('#title'),
        '#size' => 16, '#default_value' => (isset($fld_spec['description'])? $fld_spec['description'] : ''),
      );
      $form['form'][$cnt]["required-$cnt"] = array('#type' => 'select', '#title' => t('#required'),
        '#options' => array('FALSE', 'TRUE',),
      );
      $form['form'][$cnt]["default_value-$cnt"] = array('#type' => 'textfield', '#title' => t('#default_value'),
        '#size' => 16,
      );
      $form['form'][$cnt]["value-$cnt"] = array('#type' => 'textfield', '#title' => t('#value'),
        '#size' => 16,
      );
      $form['form'][$cnt]["size-$cnt"] = array('#type' => 'textfield', '#title' => t('#size'),
        '#size' => 3,
      );
      $form['form'][$cnt]["maxlength-$cnt"] = array('#type' => 'textfield', '#title' => t('#maxlength'),
        '#size' => 6, '#default_value' => (isset($fld_spec['length'])? $fld_spec['length'] : ''),
      );
      $form['form'][$cnt]["prefix-$cnt"] = array('#type' => 'textfield', '#title' => t('#prefix'),
        '#size' => 16,
      );
      $form['form'][$cnt]["suffix-$cnt"] = array('#type' => 'textfield', '#title' => t('#suffix'),
        '#size' => 16,
      );
      $form['form'][$cnt]["submit-$cnt"] = array('#type' => 'textfield', '#title' => t('#submit'),
        '#size' => 16,
      );
      $cnt++;
    }
  }
  $form['btn_submit'] = array('#type' => 'button', '#value' => t('Generate form'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_form',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  ); 
  return $form;
}

/**
 * ajax handler for codegenerator_views_form
 */
function codegenerator_forms_get_table_schema($form, $form_state) {
  return $form['form'];
}

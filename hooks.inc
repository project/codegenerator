<?php

/**
 * Generates hooks
 */
function cg_hook_form($form, &$form_state) {
  $hooks = array('menu' => t('Menu'), 'theme' => t('Theme'), 'permission' => t('Permission'),
    'cron' => t('Cron'), 'block' => t('Block'), 'node' => t('Node'));
  $user_hooks = array('user_login' => t('Login'), 'user_logout' => t('Logout'), 'user_load' => t('Load'),
    'user_insert' => t('Insert'), 'user_update' => t('Update'), 'user_delete' => t('Delete'), 
     'username_alter' => t('Name alter'));
  $form['module'] = array('#type' => 'textfield', '#title' => t('Module name'), '#required' => TRUE);
  $form['container-generic'] = array('#type' => 'container', '#attributes' => array('class' => array('container-inline')));
  $form['container-generic']['hooks'] = array('#type' => 'checkboxes', '#title' => t('Generic hooks'), '#options' => $hooks);
  $form['container-user'] = array('#type' => 'container', '#attributes' => array('class' => array('container-inline')));
  $form['container-user']['user_hooks'] = array('#type' => 'checkboxes', '#title' => t('User hooks'), '#options' => $user_hooks);
  $form['btn_get'] = array('#type' => 'submit', '#value' => t('Generate hooks'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_hooks',
      'wrapper' => 'ajax_div',
    ),
    '#prefix' => '<br/>',
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  );  
  
  return $form;
}

/**
 * ajax handler for cg_hook_form
 */
function codegenerator_get_hooks($form, &$form_state) {
  
  $output = _codegenerator_get_generic_hooks($form_state);
  $output .= _codegenerator_get_user_hooks($form_state);
  $form['ajx']['#value'] = $output;
  
  return $form['ajx'];
}

/**
 * generate stubs for user hooks
 */
function _codegenerator_get_user_hooks($form_state) {
  $hooks = array_filter($form_state['values']['user_hooks']);
  $module = $form_state['values']['module'];
  $functions = array();
  if (!empty($hooks)) {
    foreach ($hooks as $hook) {
      $comment = "/**\n * Implements hook_" . $hook . "().\n */\n";
      switch ($hook) {
        case 'user_login':
          $func = "user_login(&$" . "edit, $" . "account" . ") {\n\n}\n";
          break;
          
        case 'user_logout':
        case 'user_delete':
          $func = $hook . "($" . "account" . ") {\n\n}\n";
          break;
          
        case 'user_load':
          $func = "user_load($" . "users" . ") {\n\n}\n";
          break;
          
        case 'user_insert':
        case 'user_update':
          $func = $hook . "(&$" . "edit, $" . "account, $" . "category) {\n\n}\n";
          break;
          
        case 'username_alter':
          $func = "username_alter(&$" . "name, $" . "account" . ") {\n\n}\n";
          break;
      }
      $functions[] = $comment . "function $module" . "_" . $func;
    }
  }
  
  return implode("\n", $functions);
}

/**
 * generate stub for generic hooks
 */
function _codegenerator_get_generic_hooks($form_state) {
  $hooks = array_filter($form_state['values']['hooks']);
  $module = $form_state['values']['module'];
  $functions = array();
  if (!empty($hooks)) {
    foreach ($hooks as $hook) {
      $comment = "/**\n * Implements hook_" . $hook . "().\n */\n";
      $func = '';
      switch ($hook) {
        case 'menu':
          $func = "menu() {\n  return $" . "items;\n}\n";
          break;
          
        case 'theme':
          $func = "theme($" . "existing, $" . "type, $" . "theme, $" . "path) {\n  return array(\n   'mytheme' =>  array('render element' => 'form'),\n  );\n}\n";
          break;
          
        case 'cron':
          $func = "cron() {\n\n}\n";
          break;
          
        case 'permission':
          $func = "permission() {\n  return array();\n}\n";
          break;
      }
      if (!empty($func)) {
        $functions[] = $comment . "function $module" . "_" . $func;
      }
    }
  }
  
  _codegenerator_get_hook_sets($functions, $form_state);
  
  return implode("\n", $functions);
}

/**
 * generate related hooks for a given type
 */
function _codegenerator_get_hook_sets(&$functions, $form_state) {
  $module = $form_state['values']['module'];
  $selected_hooks = array_filter($form_state['values']['hooks']);
  foreach ($selected_hooks as $chook) {
    $hooks = array();
    switch ($chook) {
      case 'block':
        _codegenerator_get_hook_block($functions, $form_state, $module);
        break;
        
      case 'node':
        _codegenerator_get_hook_node($functions, $form_state, $module);
        break;
        
      default:
        break;
    }
  }
}

/**
 * add node related hooks
 */
function _codegenerator_get_hook_node(&$functions, $form_state, $module) {
  $hooks = array('node_info', 'load', 'node_insert', 'node_update', 'node_validate', 'node_delete', 'node_view');
  foreach ($hooks as $hook) {
    $comment = "/**\n * Implements hook_" . $hook . "().\n */\n";
    $func = '';
    switch ($hook) {
      case 'node_info':
        $func = $hook . "() {\n  $" . "node_info = array();\n" .
          "  $" . "node_info['x'] = array(\n    'name' => t('x')," .
          "\n    'base' => '$module',\n    'description' => t('x description')," .
          "\n    'has_title' => TRUE,\n  );" .          
          "\n  return $" . "node_info;\n}\n";
        break;
        
      case 'load':
        $func = $hook . "($" . "nodes) {\n\n}\n";
        break;
        
      case 'node_validate':
      case 'node_update':
      case 'node_insert':
      case 'node_delete':
        $func = $hook . "($" . "node) {\n\n}\n";
        break;
        
      case 'node_view':
        $func = $hook . "($" . "node, $" . "view_mode, $" . "langcode) {\n\n}\n";
        break;
    }
    if (!empty($func)) {
      $functions[] = $comment . "function $module" . "_" . $func;
    }
  }
}  

/**
 * add block related hookds
 */
function _codegenerator_get_hook_block(&$functions, $form_state, $module) {
  $hooks = array('block_info', 'block_view', 'block_configure', 'block_save',);
  foreach ($hooks as $hook) {
    $comment = "/**\n * Implements hook_" . $hook . "().\n */\n";
    $func = '';
    switch ($hook) {
      case 'block_info':
        $func = "block_info() {\n  $" . "blocks = array();\n" .
          "  $" . "blocks[x] = array(\n    'info' => t('x'),\n  );" .
          "\n  return $" . "blocks;\n}\n";
        break;
        
      case 'block_view':
        $func = "block_view($" . "delta = '') {\n  $" . "block = array();\n  switch($" . "delta) {\n" .
          "    case x:\n      $" . "block['subject'] = x_value;\n      $" .  "block['content'] = x_value;\n" .
          "      break;\n\n  }\n" . "  return $" . "block;\n}\n";
        break;
        
      case 'block_configure':
        $func = "block_configure($" . "delta = '') {\n  $" . "form = array();\n" .
          "\n  return $" . "form;\n}\n";
        break;
        
      case 'block_save':
        $func = "block_save($" . "delta = '', $". "edit = array()) {\n\n}\n";
        break;
    }
    if (!empty($func)) {
      $functions[] = $comment . "function $module" . "_" . $func;
    }
  }
}  



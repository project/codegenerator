<?php

/**
 * Generates permissions hook
 */
function codegenerator_permissions_form($form, &$form_state) {
  $form['perm'] = array('#theme' => 'cr_states_table');
  $cnt = 0;
  for ($cnt = 0; $cnt < 20; $cnt++) {
    $form['perm'][$cnt]["perm-$cnt"] = array('#type' => 'textfield', '#title' => t('Permission'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=perm-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['perm'][$cnt]["perm_display-$cnt"] = array('#type' => 'textfield', '#title' => t('Display text'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=perm-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['perm'][$cnt]["perm_const-$cnt"] = array('#type' => 'checkbox', '#title' => t('Constant'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=perm-$prev_cnt]"  => array ('filled' => TRUE),
    ),)
    );
    $form['perm'][$cnt]["perm_related-$cnt"] = array('#type' => 'checkboxes', '#title' => t('Generate also'),
      '#options' => array('access' => 'access', 'create' => 'create', 'edit' => 'edit', 'delete' => 'delete'),
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=perm-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    
    $prev_cnt = $cnt;
  }
  $form['btn_submit'] = array('#type' => 'submit', '#value' => t('Generate permissions'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_permissions',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  );  
    
  return $form;
}

/**
 * AJAX handler for codegenerator_permissions_form
 */
function codegenerator_get_permissions($form, &$form_state) {
  $cnt = 0;
  $values = $form_state['values'];
  $constants = array();
  $perms = array();
  while (isset($values["perm-$cnt"])) {
    if (!empty($values["perm-$cnt"])) {
      $const = '';
      $perm = $values["perm-$cnt"];
        //check if create / edit etc are needed
      $related = array_filter($values["perm_related-$cnt"]);
      if (empty($related)) {
        $needed_perms[] = $perm;
      }
      else {
        foreach ($related as $value) {
          $needed_perms[] = $value . ' ' . $perm;
        }
      }
      foreach ($needed_perms as $np) {
        //check if constant is to be generated
        if ($values["perm_const-$cnt"]) {
          $const = strtoupper($np);
          $const = str_replace(' ', '_',$const);
          $const = 'CONST_PERM_' . $const;
          $constants[$const] = "define('$const', '$np');";
        }
        else {
          $const = "'$np'";
        }
        //add this permission
        $perms[$const] = "$const => array('title' => t('" . ucwords($np) . "')),";
      }
    }
    $cnt++;
  }
  
  $output = '';
  if (!empty($constants)) {
    $output = implode("\n", $constants);
    $output .= "\n\n";
  }
  if (!empty($perms)) {
    $output .= "\n    " . implode("\n    ", $perms);
  }
  $form['ajx']['#value'] = $output;
  
  return $form['ajx'];
}

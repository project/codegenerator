<?php

/**
 * Generates data for hook_view_data
 */
function codegenerator_views_form($form, &$form_state) {
  $form['cont'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['cont']['table'] = array(
    '#type' => 'textfield',
    '#title' => t('Tablename'),
    '#required' => TRUE,
  );
  $form['cont']['btn_get_schema'] = array('#type' => 'button', '#value' => t('Get schema'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_table_schema',
      'wrapper' => 'ajax_schema_div',
    ),
  );
  $form['dmy1'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx_schema'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="ajax_schema_div">',
    '#suffix' => '</div>',
  ); 
  
  //find the different handlers available
  $handlers_path = drupal_get_path('module', 'views') . '/handlers';
  $handlers['argument'][0] = t('None');
  $handlers['filter'][0] = t('None');
  if ($handle = opendir($handlers_path)) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != '..') {
        $token = explode('.', $file);
        if ($token[1] == 'inc') {
          if (strpos($file, 'area') !== FALSE) {
            $handler = str_replace('views_handler_area', '', $token[0]);
            $handlers['area'][$token[0]] = empty($handler)? '<default>' : $handler;
          }
          elseif (strpos($file, 'filter') !== FALSE) {
            $handler = str_replace('views_handler_filter', '', $token[0]);
            $handlers['filter'][$token[0]] = empty($handler)? '<default>' : $handler;
          }
          elseif (strpos($file, 'field') !== FALSE) {
            $handler = str_replace('views_handler_field', '', $token[0]);
            $handlers['field'][$token[0]] = empty($handler)? '<default>' : $handler;
          }
          elseif (strpos($file, 'argument') !== FALSE) {
            $handler = str_replace('views_handler_argument', '', $token[0]);
            $handlers['argument'][$token[0]] = empty($handler)? '<default>' : $handler;
          }
          elseif (strpos($file, 'sort') !== FALSE) {
            $handler = str_replace('views_handler_sort', '', $token[0]);
            $handlers['sort'][$token[0]] = empty($handler)? '<default>' : $handler;
          }
        }
      }
    }
    closedir($handle);
  }
  $handlers['area']['custom'] = t('Custom');
  $handlers['field']['custom'] = t('Custom');
  $handlers['filter']['custom'] = t('Custom');
  $handlers['sort']['custom'] = t('Custom');
  $handlers['argument']['custom'] = t('Custom');
  
  if (!empty($form_state['values']['table'])) {
    $form['ajx_schema']['group'] = array(
      '#type' => 'textfield',
      '#title' => t('Group'),
      '#size' => 16,
    );
    $form['ajx_schema']['attr'] = array(
      '#type' => 'fieldset',
      '#title' => t('Base table'),
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['ajx_schema']['attr']['base'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is base table'),
      '#title_display' => 'before',
    );
    $form['ajx_schema']['attr']['base_field'] = array(
      '#type' => 'textfield',
      '#title' => t('Field'),
      '#size' => 12,
      '#states' => array('visible' => array(
        ":input[name=base]"  => array ('checked' => TRUE),
        ),
      )
    );
    $form['ajx_schema']['attr']['base_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#size' => 12,
      '#states' => array('visible' => array(
        ":input[name=base]"  => array ('checked' => TRUE),
        ),
      )
    );
    $form['ajx_schema']['attr']['base_help'] = array(
      '#type' => 'textfield',
      '#title' => t('Help'),
      '#size' => 12,
      '#states' => array('visible' => array(
        ":input[name=base]"  => array ('checked' => TRUE),
        ),
      )
    );
    
    $form['ajx_schema']['join'] = array(
      '#type' => 'fieldset',
      '#title' => t('Join details'),
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['ajx_schema']['join']['base_table'] = array(
      '#type' => 'textfield',
      '#title' => t('Base table'),
      '#size' => 12,
    );
    $form['ajx_schema']['join']['left_table'] = array(
      '#type' => 'textfield',
      '#title' => t('Left table'),
      '#size' => 12,
    );
    $form['ajx_schema']['join']['left_field'] = array(
      '#type' => 'textfield',
      '#title' => t('Left field'),
      '#size' => 12,
    );
    $form['ajx_schema']['join']['field'] = array(
      '#type' => 'textfield',
      '#title' => t('Field'),
      '#size' => 12,
    );
    $form['ajx_schema']['join']['join_type'] = array(
      '#type' => 'textfield',
      '#title' => t('Join type'),
      '#size' => 12,
    );
    $form['ajx_schema']['fields'] = array('#theme' => 'cr_form_table');
    
    $schema = drupal_get_schema($form_state['values']['table']);    
    $result  = module_invoke($schema['module'], 'schema');
    $schema = $result[$form_state['values']['table']];
    $cnt = 0;
    //put all the field relation elements in a bigger container and hide it.
    $form['ajx_schema']['relation'] = array(
      '#type' => 'container',
      '#attributes' => array('style' => 'display:none;'),
    );
    foreach ($schema['fields'] as $field => $fld_spec) {
      $form['ajx_schema']['fields'][$cnt][$field] = array(
        '#type' => 'checkbox',
        '#field_suffix' => $field,
        '#description' => t('Type: %type Description: %desc', array('%type' => $fld_spec['type'], '%desc' => (isset($fld_spec['description'])? $fld_spec['description'] : ''))),
        '#title' => t('Select field'),
      );
      $form['ajx_schema']['fields'][$cnt]["$field-field"] = array(
        '#type' => 'select',
        '#title' => t('Field handler'),
        '#options' => $handlers['field'],
        '#default_value' => 'views_handler_field',
      );
      $form['ajx_schema']['fields'][$cnt]["$field-filter"] = array(
        '#type' => 'select',
        '#title' => t('Filter handler'),
        '#options' => $handlers['filter'],
        '#default_value' => 0,
      );
      $form['ajx_schema']['fields'][$cnt]["$field-sort"] = array(
        '#type' => 'select',
        '#title' => t('Sort handler'),
        '#options' => $handlers['sort'],
        '#default_value' => 'views_handler_sort',
      );
      $form['ajx_schema']['fields'][$cnt]["$field-argument"] = array(
        '#type' => 'select',
        '#title' => t('Argument handler'),
        '#options' => $handlers['argument'],
        '#default_value' => 0,
      );
      $form['ajx_schema']['fields'][$cnt]["$field-relation-links"] = array(
        '#type' => 'container',
        '#title' => t('Relation'),
      );
      $form['ajx_schema']['fields'][$cnt]["$field-relation-links"]["$field-relation-add"] = array(
      //#states applies to item and not markup
        '#type' => 'item',
      //don't use l() to construct URL or a tag since this will encode the url and colorbox cannot handle it      
        '#markup' => "<a class='colorbox-inline' href='?width=300&height=450&inline=true#$field-container'>" . t('Add relation') .'</a>',
        '#states' => array('visible' => array(
          ":input[name=$field-relation-title]"  => array ('empty' => TRUE),
          )
        ),
      );
      $form['ajx_schema']['fields'][$cnt]["$field-relation-links"]["$field-relation-edit"] = array(
      //#states applies to item and not markup
        '#type' => 'item',
      //don't use l() to construct URL or a tag since this will encode the url and colorbox cannot handle it
        '#markup' => "<a class='colorbox-inline' href='?width=3000&height=450&inline=true#$field-container'>" . t('Edit relation') .'</a>',
        '#states' => array('visible' => array(
          ":input[name=$field-relation-title]"  => array ('empty' => FALSE),
          )),
      );
      $form['ajx_schema']['relation']["$field-relation"] = array(
        '#type' => 'container',
        '#prefix' => "<div id='$field-container'>",
        '#suffix' => '</div>'
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-desc"] = array(
        '#markup' => t('Relation for %field. <br/><b>Note all field are mandatory</b>', array('%field' => $field)),
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-title"] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#size' => '16',
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-help"] = array(
        '#type' => 'textfield',
        '#title' => t('Help'),
        '#size' => '32',
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-base_table"] = array(
        '#type' => 'textfield',
        '#title' => t('Base table'),
        '#size' => '16',
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-base_field"] = array(
        '#type' => 'textfield',
        '#title' => t('Base field'),
        '#size' => '16',
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-field"] = array(
        '#type' => 'textfield',
        '#title' => t('Field'),
        '#size' => '16',
      );
      $form['ajx_schema']['relation']["$field-relation"]["$field-relation-label"] = array(
        '#type' => 'textfield',
        '#title' => t('Label'),
        '#size' => '16',
      );
      $cnt++;
    }
  }
  $form['btn_submit'] = array('#type' => 'button', '#value' => t('Generate views data'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_views_data',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  ); 
  
  return $form;
}

/**
 * validate handler
 */
function codegenerator_views_form_validate($form, &$form_state) {
  $schema = drupal_get_schema($form_state['values']['table'], TRUE);
  if (!$schema) {
    form_set_error('table', t('Table not found.'));
  }
}

/**
 * ajax handler for codegenerator_views_form
 */
function codegenerator_get_table_schema($form, $form_state) {
  return $form['ajx_schema'];
}

/**
 * ajax handler for codegenerator_views_form
 */
function codegenerator_get_views_data($form, $form_state) {
  $table = $form_state['values']['table'];
  $schema = drupal_get_schema($table);
  $prefix =  "/**" . "\n" . " * Views data definition for table $table" . "\n" . " */" . "\n" .
  "function _$table" . '_views_data(&$data) {';
  $suffix = '}';
  if ($schema) {
    $views = array();
    if (!empty($form_state['values']['group'])) {
      $views[] = "$" . "data['$table']['table']['group']  = t('" . $form_state['values']['group'] . "');";
    }
    if ($form_state['values']['base']) {
      $views[] = "$" . "data['$table']['table']['base'] = array(" .
        "\n    'field' => '" . $form_state['values']['base_field'] . "'," .
        "\n    'title' => t('" . $form_state['values']['base_title'] . "')," .
        "\n    'help' => t('". $form_state['values']['base_help'] . "')," .
        "\n  );";
    }
    if (!empty($form_state['values']['base_table'])) {
      $views[] = "$" . "data['$table']['table']['join']['" . $form_state['values']['base_table'] . "'] = array(" .
        ((!empty($form_state['values']['left_table']))? "\n    'left_table' => '" . $form_state['values']['left_table'] . "',"  : '' ) .    
        "\n    'left_field' => '". $form_state['values']['left_field'] . "'," .
        "\n    'field' => '" . $form_state['values']['field'] . "'," .
        ((!empty($form_state['values']['join_type']))? "\n    'type' => '" . $form_state['values']['join_type'] . "',"  : '' ) .
        "\n  );";
    }
    //refetch the table schema as 'description' is not returned by the drupal_get_schema function.
    $module = $schema['module'];
    $module_schema = call_user_func($module . '_schema');
    $schema = $module_schema[$table];
    foreach ($schema['fields'] as $field => $fld_spec) {
      if ($form_state['values'][$field]) {
        $field_handler = $form_state['values']["$field-field"];
        if (!empty($form_state['values']["$field-filter"])) {
          $filter_handler = "    'filter' => array(\n      'handler' => '". $form_state['values']["$field-filter"] . "',\n    ),\n"; 
        }
        else {
          $filter_handler = '';
        }
        $sort_handler = $form_state['values']["$field-sort"];
        //is field exposed as argument
        if (!empty($form_state['values']["$field-argument"])) {
          $argument_handler = "    'argument' => array(\n      'handler' => '" . $form_state['values']["$field-argument"]  . "'," .
            "\n      'name field' => '" . (empty($schema['fields'][$field]['description'])? $field  : $schema['fields'][$field]['description']) . "',";
          if ($schema['fields'][$field]['type'] == 'int' || $schema['fields'][$field]['type'] == 'numeric' 
            || $schema['fields'][$field]['type'] == 'float' || $schema['fields'][$field]['type'] == 'serial') {
           $argument_handler .= "\n      'numeric' => TRUE,";
          }  
          $argument_handler .= "\n    ),\n";
        }
        else {
          $argument_handler = '';
        }
        //does field have relationship
        if (!empty($form_state['values']["$field-relation-title"]) &&
          !empty($form_state['values']["$field-relation-help"]) &&
          !empty($form_state['values']["$field-relation-base_table"]) &&
          !empty($form_state['values']["$field-relation-base_field"]) &&
          !empty($form_state['values']["$field-relation-field"]) &&
          !empty($form_state['values']["$field-relation-label"])) {
          $relation =  "    'relationship' => array(" .
            "\n      'title' => t('" . $form_state['values']["$field-relation-title"] . "')," .
            "\n      'help' => t('" . $form_state['values']["$field-relation-help"] . "')," .
            "\n      'handler' => 'views_handler_relationship'," .
            "\n      'base' => '" . $form_state['values']["$field-relation-base_table"] . "'," .
            "\n      'base field' => '" . $form_state['values']["$field-relation-base_field"] . "'," .
            "\n      'field' => '" . $form_state['values']["$field-relation-field"] . "'," .
            "\n      'label' => t('" . $form_state['values']["$field-relation-label"] . "')," .
            "\n    ),\n";
        }
        else {
          $relation = '';
        }
        //build the field
        $title = empty($fld_spec['description'])? str_replace('_', ' ', $field) : $fld_spec['description'];
        $views[] = "$" . "data['$table']['$field'] = array(\n" .
          "    'title' => t('" . ucwords($title) . "'),\n" .
          "    'help' => t('" . ucwords($fld_spec['description']) . "'),\n" .
          "    'field' => array(\n      'handler' => '$field_handler',\n      'click sortable' => TRUE,\n    ),\n" .
          $filter_handler .
          "    'sort' => array(\n      'handler' => '$sort_handler',\n    ),\n" .
          $argument_handler .
          $relation .
          "  );";
      }
    }
    if (empty($views)) {
      drupal_set_message(t('Select fields to add to the views data handler.'), 'error');
    }
    $form['ajx']['#value'] = $prefix . "\n" . '  ' . implode("\n  ", $views) . "\n" . $suffix;
  }
  return $form['ajx'];
}

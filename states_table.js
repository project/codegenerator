(function($){
  $(document).ready(function(){
	  $('.states_cell').bind('change', showHideRows);
	  showHideRows();
  }); //end of document ready
  
  var showHideRows = function(){
		$('.states_row').each(function(){
			var id = $(this).attr('id');
			var visible = false;
			$('.' + id).each(function(){
			  //if it's checkboxes, don't check the display status. check for other elements.				
			  if ($(this).children('.form-type-checkbox').length == 0){
			    var parent = $(this).parent();
			    if (parent.css('display') != 'none') {
				  visible = true;
			    }
			  }
			});
			if (visible) {
			  $(this).css('display', '');
			}
			else {
			  $(this).css('display', 'none');
			}
		});
  };
  //insert all code before this.
})(jQuery);	

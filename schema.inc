<?php
/**
 * Generate schema
 */
function codegenerator_schema_form($form, &$form_state) {
  $form['ctn'] = array('#type' => 'container', '#attributes' => array('class' => array('container-inline')));
  $form['ctn']["table_name"] = array('#type' => 'textfield', '#title' => t('Table name'), '#size' => 16);
  $form['ctn']["table_desc"] = array('#type' => 'textfield', '#title' => t('Table description'), '#size' => 16);
  $form['ctn']["primary_key"] = array('#type' => 'textfield', '#title' => t('Primary key'), '#size' => 16);
  $field_types = array('char' => 'char', 'varchar' => 'varchar', 'text' => 'text',
    'blob' => 'blob', 'int' => 'int', 'float' => 'float', 'numeric' => 'numeric', 'serial' => 'serial');
  $field_size = array('' => '', 'tiny' => 'tiny', 'small' => 'small', 'medium' => 'medium',
    'normal' => 'normal', 'big' => 'big',);
  $form['field'] = array('#theme' => 'cr_states_table');
  for ($cnt = 0; $cnt < 30; $cnt++) {
    $form['field'][$cnt]["field_name-$cnt"] = array('#type' => 'textfield', '#title' => t('Field name'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_type-$cnt"] = array('#type' => 'select', '#title' => t('Type'),
      '#options' => $field_types,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_desc-$cnt"] = array('#type' => 'textfield', '#title' => t('Field description'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_null-$cnt"] = array('#type' => 'checkbox', '#title' => t('Not null'),
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_unsigned-$cnt"] = array('#type' => 'checkbox', '#title' => t('Unsigned'),
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_length-$cnt"] = array('#type' => 'textfield', '#title' => t('Length'),
      '#size' => 6,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_size-$cnt"] = array('#type' => 'select', '#title' => t('Size'),
      '#options' => $field_size,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_ps-$cnt"] = array('#type' => 'textfield', '#title' => t('Precision, Scale'),
      '#size' => 6,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_default-$cnt"] = array('#type' => 'textfield', '#title' => t('Default'),
      '#size' => 6,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['field'][$cnt]["field_serialize-$cnt"] = array('#type' => 'checkbox', '#title' => t('Serialize'),
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=field_name-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $prev_cnt = $cnt;
  }
  
  $form['btn_submit'] = array('#type' => 'submit', '#value' => t('Get table definition'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_schema',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['btn_flds'] = array('#type' => 'submit', '#value' => t('Get field definition'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_field',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  );  
  
  return $form;
}

function codegenerator_get_schema($form, &$form_state) {
  $cnt = 0;
  $values = $form_state['values'];
  $primary_key = '';
  $fields = array();
  while (isset($values["field_name-$cnt"])) {
    $field_name = $values["field_name-$cnt"];
    if (!empty($field_name)) {      
      $fields[] = _codegenerator_get_fieldspec($values, $field_name, $cnt);
    }
    $cnt++;
  }
  
  if (!empty($fields)) {
    $table_name = $values['table_name'];
    $table_desc = $values['table_desc'];
    $output = "  $" . "schema['" . $table_name . "'] = array(\n" . 
      "    'description' => '$table_desc', \n" .
      "    'fields' => array(\n      " . implode(",\n      ", $fields) . "\n    ), ";
    
    if (!empty($values['primary_key'])) {
      $primary_key = $values['primary_key'];
    }
    
    if (!empty($primary_key)) {
      $output .= "\n    'primary key' => array('" . $primary_key . "'),";
    }
    //TODO indexes
    $output .= "\n  );";
    $form['ajx']['#value'] = $output;
  }
  
  return $form['ajx'];
}

function codegenerator_get_field($form, &$form_state) {
  $cnt = 0;
  $values = $form_state['values'];
  $primary_key = '';
  $fields = array();
  while (isset($values["field_name-$cnt"])) {
    $field_name = $values["field_name-$cnt"];
    if (!empty($field_name)) {      
      $fields[] = _codegenerator_get_fieldspec($values, $field_name, $cnt);
    }
    $cnt++;
  }  
  $form['ajx']['#value'] = implode(",\n      ", $fields);
  return $form['ajx']; 
}

function _codegenerator_get_fieldspec($values, $field_name, $cnt) {
  $field_spec = array();
  $field_spec[] = "'description' => '" . $values["field_desc-$cnt"] . "'";
  $field_spec[] = "'type' => '" . $values["field_type-$cnt"] . "'";
  $field_spec[] = "'not null' => " . (empty($values["field_null-$cnt"])? 'FALSE' : 'TRUE');
  if (is_numeric($values["field_default-$cnt"])) {
    $field_spec[] = "'default' => " . $values["field_default-$cnt"];
  }
  if (!empty($values["field_size-$cnt"])) {
    $field_spec[] = "'size' => '" . $values["field_size-$cnt"] . "'";
  }
  switch ($values["field_type-$cnt"]) {
    case 'serial':
      $primary_key = $field_name;
    case 'numeric':
      if (!empty($values["field_ps-$cnt"])) {
        $ps = explode(',', $values["field_ps-$cnt"]); 
        $field_spec[] = "'precision' => " . $ps[0];
        $field_spec[] = "'scale' => " . $ps[1];
      }
      //intentionally no break statement.
    case 'int':
      $field_spec[] = "'unsigned' => " . (empty($values["field_unsigned-$cnt"])? 'FALSE' : 'TRUE');
      break;
      
    case 'varchar':
    case 'char':
    case 'blob':
      if (!empty($values["field_length-$cnt"])) {
        $field_spec[] = "'length' => " . $values["field_length-$cnt"];
      }
      break;
  }
  return "'$field_name' => array(\n        " . implode(",\n        ", $field_spec) . ",\n      )";  
}

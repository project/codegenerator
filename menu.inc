<?php

function codegenerator_menu_form($form, &$form_state) {

  $form['menu'] = array('#theme' => 'cr_states_table');
  $cnt = 0;
  $menu_options = array('MENU_NORMAL_ITEM' => 'MENU_NORMAL_ITEM', 'MENU_CALLBACK' => 'MENU_CALLBACK',
        'MENU_SUGGESTED_ITEM' => 'MENU_SUGGESTED_ITEM', 'MENU_LOCAL_ACTION' => 'MENU_LOCAL_ACTION',
        'MENU_LOCAL_TASK' => 'MENU_LOCAL_TASK', 'MENU_DEFAULT_LOCAL_TASK' => 'MENU_DEFAULT_LOCAL_TASK');
  for ($cnt = 0; $cnt < 20; $cnt++) {
    $form['menu'][$cnt]["menu_path-$cnt"] = array('#type' => 'textfield', '#title' => t('Menu path'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_title-$cnt"] = array('#type' => 'textfield', '#title' => t('Menu title'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_desc-$cnt"] = array('#type' => 'textfield', '#title' => t('Menu description'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_pagecallback-$cnt"] = array('#type' => 'textfield', '#title' => t('Page callback'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_pagearguments-$cnt"] = array('#type' => 'textfield', '#title' => t('Page arguments'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_accesscallback-$cnt"] = array('#type' => 'textfield', '#title' => t('Access callback'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    $form['menu'][$cnt]["menu_accessarguments-$cnt"] = array('#type' => 'textfield', '#title' => t('Access arguments'),
      '#size' => 16,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    
    $form['menu'][$cnt]["menu_type-$cnt"] = array('#type' => 'select', '#title' => t('Type'),
      '#options' => $menu_options,
      '#states' => empty($cnt) ? array() :  array('visible' => array(
        ":input[name=menu_path-$prev_cnt]"  => array ('filled' => TRUE),
    ),));
    
    $prev_cnt = $cnt;
  }
  $form['btn_submit'] = array('#type' => 'submit', '#value' => t('Generate menu'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_menu',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['btn_func'] = array('#type' => 'submit', '#value' => t('Generate function'),
    '#ajax' => array(
      'callback' => 'codegenerator_get_func',
      'wrapper' => 'ajax_div',
    ),
  );
  $form['dmy'] = array('#prefix' => '<div style="clear:both">', '#suffix' => '</div>');
  $form['ajx'] = array(
    '#type' => 'textarea',
    '#prefix' => '<div id="ajax_div">',
    '#suffix' => '</div>',
  );  
  
  return $form;
}

/**
 * Generates the menu items
 */
function codegenerator_get_menu($form, &$form_state) {
  $cnt = 0;
  $menus = array();
  $values = $form_state['values'];
  $boolean = array('TRUE', 'FALSE',);
  while(isset($values["menu_path-$cnt"])) {
    $path = $values["menu_path-$cnt"];
    if (!empty($path)) {
      $menu_spec = array();
      $menu_spec[] = "'title' => '" . $values["menu_title-$cnt"] . "'";
      if (!empty($values["menu_desc-$cnt"])) {
        $menu_spec[] = "'description' => '" . $values["menu_desc-$cnt"] . "'";
      }
      if (!empty($values["menu_pagecallback-$cnt"])) {
        if (in_array(strtoupper($values["menu_pagecallback-$cnt"]), $boolean)) {
          $menu_spec[] = "'page callback' => " . strtoupper($values["menu_pagecallback-$cnt"]);
        }
        else {
          $menu_spec[] = "'page callback' => '" . $values["menu_pagecallback-$cnt"] . "'";
        }
      }
      if (!empty($values["menu_pagearguments-$cnt"])) {
        //if the call back is drupal_get_form, then the first argument must be enclosed in quotes
        if ('drupal_get_form' == $values["menu_pagecallback-$cnt"] &&
           strpos($values["menu_pagearguments-$cnt"], "'") === FALSE) {
          $token = explode(',', $values["menu_pagearguments-$cnt"]);
          $token[0] = "'" . $token[0] . "'";
          $values["menu_pagearguments-$cnt"] = implode(', ', $token);
        }
        $menu_spec[] = "'page arguments' => array(" . $values["menu_pagearguments-$cnt"] . ")";
      }
      
      if (!empty($values["menu_accesscallback-$cnt"])) {      
        if (in_array(strtoupper($values["menu_accesscallback-$cnt"]), $boolean)) {
          $menu_spec[] = "'access callback' => " . strtoupper($values["menu_accesscallback-$cnt"]);
        }
        else {
          $menu_spec[] = "'access callback' => '" . $values["menu_accesscallback-$cnt"] . "'";
        }
      }
      if (!empty($values["menu_accessarguments-$cnt"])) {
        $menu_spec[] = "'access arguments' => array(" . $values["menu_accessarguments-$cnt"] . ")";
      }
      $menu_spec[] = "'type' => " . $values["menu_type-$cnt"];
      //if menu type is MENU_DEFAULT_LOCAL_TASK add weight = -10
      if ($values["menu_type-$cnt"] == 'MENU_DEFAULT_LOCAL_TASK') {
        $menu_spec[] = "'weight' => -10";
      }
      
      $menus[] = "$" . "items['" . $values["menu_path-$cnt"] . "'] = array(\n    " .
        implode(",\n    ", $menu_spec) .
        ",\n  )";
    }
    $cnt++;
  }
  if (!empty($menus)) {
    $form['ajx']['#value'] = implode(";\n  ", $menus) . ';';
  }
  return $form['ajx'];
}

/**
 * Generate the function stubs from the menu definitions
 */
function codegenerator_get_func($form, &$form_state) {
  $cnt = 0;
  $values = $form_state['values'];
  $boolean = array('TRUE', 'FALSE',);
  $func_names = array();
  while(isset($values["menu_path-$cnt"])) {
    if (!empty($values["menu_accesscallback-$cnt"]) &&
      !in_array(strtoupper($values["menu_accesscallback-$cnt"]), $boolean)) {      
        $func_names[] = "/**\n * Access callback for menu " . $values["menu_path-$cnt"] . "\n */\n" . 'function ' . $values["menu_accesscallback-$cnt"] . "() {\n\n}" ;
    }
    if (!empty($values["menu_pagecallback-$cnt"]) &&
      !in_array(strtoupper($values["menu_pagecallback-$cnt"]), $boolean)) {      
        if ('drupal_get_form' == $values["menu_pagecallback-$cnt"]) {
          //if the call back is drupal_get_form, then the first argument must be enclosed in quotes.
          //check if it's missing or not and generate function appropriately
          if (strpos($values["menu_pagearguments-$cnt"], "'") === FALSE) {
            $token[1] = $values["menu_pagearguments-$cnt"];
          }
          else {
            $token = explode("'", $values["menu_pagearguments-$cnt"]);
          }
          if (!empty($values["menu_desc-$cnt"])) {
            $cmt = "/**\n * " . $values["menu_desc-$cnt"] . "\n */\n";
          }
          else {
            $cmt = '';
          }
          $func_names[] = $cmt . 'function ' . $token[1] . "($" . "form, &$" . "form_state) {\n\n  return $" . "form;\n}" ;;
          //add submit & validate functions
          $func_names[] = "/**\n * Validate handler for " . $token[1] . "\n */\n" . 'function ' . $token[1] . '_validate' . "($" . "form, &$" . "form_state) {\n\n}" ;;
          $func_names[] = "/**\n * Submit handler for " . $token[1] . "\n */\n" . 'function ' . $token[1] . '_submit' . "($" . "form, &$" . "form_state) {\n\n}" ;;
        }
        else {
          $func_names[] = 'function ' . $values["menu_pagecallback-$cnt"] . "() {\n\n}" ;
        }
    }
    $cnt++;
  }
  
  if (!empty($func_names)) {
    $form['ajx']['#value'] = implode("\n\n", $func_names) . "\n";
  }
  return $form['ajx'];
}
